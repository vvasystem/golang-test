FROM debian:9.3

ENV SERVICE_PORT 80

COPY golang-test /srv/golang-test/

RUN chmod -R +x /srv/golang-test/

WORKDIR /srv/golang-test

CMD ["/srv/golang-test/golang-test"]

EXPOSE $SERVICE_PORT