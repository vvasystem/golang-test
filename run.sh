#!/bin/sh
sudo docker run -d \
    -p 80:80 \
    --restart always \
    --name golang-test \
    golang-test
